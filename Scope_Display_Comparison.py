#!/usr/bin/env python
##################################################
# Gnuradio Python Flow Graph
# Title: Scope Display Comparison
# Description: Compare 2-channels vs Re-Im Scope-> Re-Im is more precise
# Generated: Fri May 10 10:23:53 2013
##################################################

from gnuradio import eng_notation
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.gr import firdes
from gnuradio.wxgui import scopesink2
from grc_gnuradio import wxgui as grc_wxgui
from optparse import OptionParser
import wx

class Scope_Display_Comparison(grc_wxgui.top_block_gui):

	def __init__(self):
		grc_wxgui.top_block_gui.__init__(self, title="Scope Display Comparison")
		_icon_path = "/usr/share/icons/hicolor/32x32/apps/gnuradio-grc.png"
		self.SetIcon(wx.Icon(_icon_path, wx.BITMAP_TYPE_ANY))

		##################################################
		# Variables
		##################################################
		self.samp_rate = samp_rate = 10*1000
		self.n_repeat = n_repeat = 10
		self.fc = fc = 3*1000

		##################################################
		# Blocks
		##################################################
		self.notebook_0 = self.notebook_0 = wx.Notebook(self.GetWin(), style=wx.NB_TOP)
		self.notebook_0.AddPage(grc_wxgui.Panel(self.notebook_0), "Complex")
		self.notebook_0.AddPage(grc_wxgui.Panel(self.notebook_0), "Complex-2")
		self.Add(self.notebook_0)
		self.wxgui_scopesink2_0_0_0_0_0 = scopesink2.scope_sink_c(
			self.notebook_0.GetPage(1).GetWin(),
			title="Scope Plot",
			sample_rate=samp_rate,
			v_scale=0,
			v_offset=0,
			t_scale=0,
			ac_couple=False,
			xy_mode=False,
			num_inputs=1,
			trig_mode=gr.gr_TRIG_MODE_AUTO,
			y_axis_label="Counts",
		)
		self.notebook_0.GetPage(1).Add(self.wxgui_scopesink2_0_0_0_0_0.win)
		self.wxgui_scopesink2_0_0_0_0 = scopesink2.scope_sink_c(
			self.notebook_0.GetPage(0).GetWin(),
			title="Scope Plot",
			sample_rate=samp_rate,
			v_scale=0,
			v_offset=0,
			t_scale=0,
			ac_couple=False,
			xy_mode=False,
			num_inputs=1,
			trig_mode=gr.gr_TRIG_MODE_AUTO,
			y_axis_label="Counts",
		)
		self.notebook_0.GetPage(0).Add(self.wxgui_scopesink2_0_0_0_0.win)
		self.low_pass_filter_0 = gr.fir_filter_fff(1, firdes.low_pass(
			1, samp_rate, fc-fc/200, fc/100, firdes.WIN_HAMMING, 6.76))
		self.gr_vector_source_x_0 = gr.vector_source_f((1,0,0,0,0,0,0,0,0,0,0), True, 1)
		self.gr_sig_source_x_0 = gr.sig_source_f(samp_rate, gr.GR_SIN_WAVE, fc, 1, 0)
		self.gr_repeat_0 = gr.repeat(gr.sizeof_float*1, n_repeat)
		self.gr_multiply_xx_0_0 = gr.multiply_vff(1)
		self.gr_multiply_xx_0 = gr.multiply_vff(1)
		self.gr_float_to_complex_0_0 = gr.float_to_complex(1)
		self.gr_float_to_complex_0 = gr.float_to_complex(1)
		self.gr_delay_0 = gr.delay(gr.sizeof_float*1, 10)

		##################################################
		# Connections
		##################################################
		self.connect((self.gr_repeat_0, 0), (self.gr_float_to_complex_0_0, 0))
		self.connect((self.gr_repeat_0, 0), (self.gr_float_to_complex_0, 0))
		self.connect((self.gr_vector_source_x_0, 0), (self.gr_repeat_0, 0))
		self.connect((self.gr_float_to_complex_0_0, 0), (self.wxgui_scopesink2_0_0_0_0_0, 0))
		self.connect((self.gr_float_to_complex_0, 0), (self.wxgui_scopesink2_0_0_0_0, 0))
		self.connect((self.gr_delay_0, 0), (self.gr_float_to_complex_0, 1))
		self.connect((self.gr_sig_source_x_0, 0), (self.gr_multiply_xx_0, 0))
		self.connect((self.gr_repeat_0, 0), (self.gr_multiply_xx_0, 1))
		self.connect((self.gr_multiply_xx_0, 0), (self.gr_delay_0, 0))
		self.connect((self.gr_delay_0, 0), (self.gr_multiply_xx_0_0, 0))
		self.connect((self.gr_sig_source_x_0, 0), (self.gr_multiply_xx_0_0, 1))
		self.connect((self.gr_multiply_xx_0_0, 0), (self.low_pass_filter_0, 0))
		self.connect((self.low_pass_filter_0, 0), (self.gr_float_to_complex_0_0, 1))

	def get_samp_rate(self):
		return self.samp_rate

	def set_samp_rate(self, samp_rate):
		self.samp_rate = samp_rate
		self.gr_sig_source_x_0.set_sampling_freq(self.samp_rate)
		self.wxgui_scopesink2_0_0_0_0.set_sample_rate(self.samp_rate)
		self.wxgui_scopesink2_0_0_0_0_0.set_sample_rate(self.samp_rate)
		self.low_pass_filter_0.set_taps(firdes.low_pass(1, self.samp_rate, self.fc-self.fc/200, self.fc/100, firdes.WIN_HAMMING, 6.76))

	def get_n_repeat(self):
		return self.n_repeat

	def set_n_repeat(self, n_repeat):
		self.n_repeat = n_repeat

	def get_fc(self):
		return self.fc

	def set_fc(self, fc):
		self.fc = fc
		self.gr_sig_source_x_0.set_frequency(self.fc)
		self.low_pass_filter_0.set_taps(firdes.low_pass(1, self.samp_rate, self.fc-self.fc/200, self.fc/100, firdes.WIN_HAMMING, 6.76))

if __name__ == '__main__':
	parser = OptionParser(option_class=eng_option, usage="%prog: [options]")
	(options, args) = parser.parse_args()
	tb = Scope_Display_Comparison()
	tb.Run(True)

