#!/usr/bin/env python
##################################################
# Gnuradio Python Flow Graph
# Title: Top Block
# Generated: Thu Oct  2 08:29:38 2014
##################################################

from gnuradio import audio
from gnuradio import eng_notation
from gnuradio import fft
from gnuradio import gr
from gnuradio import window
from gnuradio.eng_option import eng_option
from gnuradio.gr import firdes
from gnuradio.wxgui import forms
from gnuradio.wxgui import scopesink2
from gnuradio.wxgui import waterfallsink2
from grc_gnuradio import wxgui as grc_wxgui
from optparse import OptionParser
import wx

class top_block(grc_wxgui.top_block_gui):

	def __init__(self):
		grc_wxgui.top_block_gui.__init__(self, title="Top Block")
		_icon_path = "/usr/share/icons/hicolor/32x32/apps/gnuradio-grc.png"
		self.SetIcon(wx.Icon(_icon_path, wx.BITMAP_TYPE_ANY))

		##################################################
		# Variables
		##################################################
		self.s_len = s_len = 64
		self.n_repeat = n_repeat = 1*10
		self.v_sound = v_sound = 343.2
		self.samp_rate = samp_rate = 100000
		self.n_shift = n_shift = 50
		self.fc = fc = 30*1000
		self.N_DATA = N_DATA = s_len*n_repeat
		self.Gr = Gr = 3*100

		##################################################
		# Blocks
		##################################################
		self.notebook_0 = self.notebook_0 = wx.Notebook(self.GetWin(), style=wx.NB_TOP)
		self.notebook_0.AddPage(grc_wxgui.Panel(self.notebook_0), "Tx")
		self.notebook_0.AddPage(grc_wxgui.Panel(self.notebook_0), "Rx")
		self.notebook_0.AddPage(grc_wxgui.Panel(self.notebook_0), "B-Scan")
		self.Add(self.notebook_0)
		_n_shift_sizer = wx.BoxSizer(wx.VERTICAL)
		self._n_shift_text_box = forms.text_box(
			parent=self.GetWin(),
			sizer=_n_shift_sizer,
			value=self.n_shift,
			callback=self.set_n_shift,
			label='n_shift',
			converter=forms.float_converter(),
			proportion=0,
		)
		self._n_shift_slider = forms.slider(
			parent=self.GetWin(),
			sizer=_n_shift_sizer,
			value=self.n_shift,
			callback=self.set_n_shift,
			minimum=0,
			maximum=N_DATA,
			num_steps=N_DATA,
			style=wx.SL_HORIZONTAL,
			cast=float,
			proportion=1,
		)
		self.Add(_n_shift_sizer)
		self.wxgui_waterfallsink2_0 = waterfallsink2.waterfall_sink_c(
			self.notebook_0.GetPage(2).GetWin(),
			baseband_freq=0,
			dynamic_range=100,
			ref_level=0,
			ref_scale=2.0,
			sample_rate=samp_rate,
			fft_size=N_DATA,
			fft_rate=30,
			average=False,
			avg_alpha=None,
			title="B-Scan Image",
		)
		self.notebook_0.GetPage(2).Add(self.wxgui_waterfallsink2_0.win)
		self.wxgui_scopesink2_0_0 = scopesink2.scope_sink_f(
			self.notebook_0.GetPage(0).GetWin(),
			title="Scope Plot",
			sample_rate=samp_rate,
			v_scale=0,
			v_offset=0,
			t_scale=0,
			ac_couple=False,
			xy_mode=False,
			num_inputs=1,
			trig_mode=gr.gr_TRIG_MODE_AUTO,
			y_axis_label="Counts",
		)
		self.notebook_0.GetPage(0).Add(self.wxgui_scopesink2_0_0.win)
		self.wxgui_scopesink2_0 = scopesink2.scope_sink_c(
			self.notebook_0.GetPage(1).GetWin(),
			title="Scope Plot",
			sample_rate=samp_rate,
			v_scale=0,
			v_offset=0,
			t_scale=0,
			ac_couple=False,
			xy_mode=False,
			num_inputs=1,
			trig_mode=gr.gr_TRIG_MODE_AUTO,
			y_axis_label="Counts",
		)
		self.notebook_0.GetPage(1).Add(self.wxgui_scopesink2_0.win)
		self.low_pass_filter_0 = gr.fir_filter_ccf(1, firdes.low_pass(
			2, samp_rate, 0.95*fc, 1000, firdes.WIN_HAMMING, 6.76))
		self.gr_vector_to_stream_1 = gr.vector_to_stream(gr.sizeof_gr_complex*1, N_DATA)
		self.gr_vector_source_x_0 = gr.vector_source_f(1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, True, 1, [])
		    
		self.gr_stream_to_vector_1 = gr.stream_to_vector(gr.sizeof_gr_complex*1, N_DATA)
		self.gr_sig_source_x_0 = gr.sig_source_c(samp_rate, gr.GR_COS_WAVE, fc, 1, 0)
		self.gr_repeat_0 = gr.repeat(gr.sizeof_float*1, n_repeat)
		self.gr_multiply_xx_1_1 = gr.multiply_vff(1)
		self.gr_multiply_xx_1_0_0 = gr.multiply_vff(1)
		self.gr_multiply_xx_1_0 = gr.multiply_vff(1)
		self.gr_multiply_xx_1 = gr.multiply_vff(1)
		self.gr_multiply_const_vxx_1 = gr.multiply_const_vff((-1, ))
		self.gr_multiply_const_vxx_0 = gr.multiply_const_vff((Gr, ))
		self.gr_float_to_complex_1 = gr.float_to_complex(1)
		self.gr_float_to_complex_0 = gr.float_to_complex(1)
		self.gr_delay_0 = gr.delay(gr.sizeof_float*1, n_shift)
		self.gr_complex_to_mag_squared_0 = gr.complex_to_mag_squared(1)
		self.gr_complex_to_float_0 = gr.complex_to_float(1)
		self.gr_add_xx_0 = gr.add_vff(1)
		self.fft_vxx_1 = fft.fft_vcc(N_DATA, False, (window.blackmanharris(N_DATA)), True, 1)
		self.audio_source_0 = audio.source(samp_rate, "", True)
		self.audio_sink_0 = audio.sink(samp_rate, "", True)

		##################################################
		# Connections
		##################################################
		self.connect((self.gr_vector_source_x_0, 0), (self.gr_repeat_0, 0))
		self.connect((self.gr_repeat_0, 0), (self.gr_multiply_xx_1, 0))
		self.connect((self.gr_float_to_complex_0, 0), (self.wxgui_scopesink2_0, 0))
		self.connect((self.audio_source_0, 0), (self.gr_multiply_const_vxx_0, 0))
		self.connect((self.gr_multiply_const_vxx_0, 0), (self.gr_multiply_xx_1_0, 1))
		self.connect((self.gr_sig_source_x_0, 0), (self.gr_complex_to_float_0, 0))
		self.connect((self.gr_complex_to_float_0, 0), (self.gr_multiply_xx_1, 1))
		self.connect((self.gr_complex_to_float_0, 0), (self.gr_multiply_xx_1_0, 0))
		self.connect((self.gr_multiply_const_vxx_0, 0), (self.gr_multiply_xx_1_0_0, 1))
		self.connect((self.gr_float_to_complex_1, 0), (self.low_pass_filter_0, 0))
		self.connect((self.low_pass_filter_0, 0), (self.gr_complex_to_mag_squared_0, 0))
		self.connect((self.gr_complex_to_mag_squared_0, 0), (self.gr_float_to_complex_0, 1))
		self.connect((self.gr_repeat_0, 0), (self.gr_multiply_xx_1_1, 0))
		self.connect((self.gr_multiply_xx_1, 0), (self.gr_add_xx_0, 0))
		self.connect((self.gr_multiply_xx_1_1, 0), (self.gr_add_xx_0, 1))
		self.connect((self.gr_add_xx_0, 0), (self.audio_sink_0, 0))
		self.connect((self.gr_add_xx_0, 0), (self.wxgui_scopesink2_0_0, 0))
		self.connect((self.gr_multiply_xx_1_0_0, 0), (self.gr_float_to_complex_1, 1))
		self.connect((self.gr_multiply_xx_1_0, 0), (self.gr_float_to_complex_1, 0))
		self.connect((self.gr_repeat_0, 0), (self.gr_delay_0, 0))
		self.connect((self.gr_delay_0, 0), (self.gr_float_to_complex_0, 0))
		self.connect((self.gr_complex_to_float_0, 1), (self.gr_multiply_const_vxx_1, 0))
		self.connect((self.gr_multiply_const_vxx_1, 0), (self.gr_multiply_xx_1_0_0, 0))
		self.connect((self.gr_multiply_const_vxx_1, 0), (self.gr_multiply_xx_1_1, 1))
		self.connect((self.fft_vxx_1, 0), (self.gr_vector_to_stream_1, 0))
		self.connect((self.gr_stream_to_vector_1, 0), (self.fft_vxx_1, 0))
		self.connect((self.gr_vector_to_stream_1, 0), (self.wxgui_waterfallsink2_0, 0))
		self.connect((self.low_pass_filter_0, 0), (self.gr_stream_to_vector_1, 0))


	def get_s_len(self):
		return self.s_len

	def set_s_len(self, s_len):
		self.s_len = s_len
		self.set_N_DATA(self.s_len*self.n_repeat)

	def get_n_repeat(self):
		return self.n_repeat

	def set_n_repeat(self, n_repeat):
		self.n_repeat = n_repeat
		self.set_N_DATA(self.s_len*self.n_repeat)

	def get_v_sound(self):
		return self.v_sound

	def set_v_sound(self, v_sound):
		self.v_sound = v_sound

	def get_samp_rate(self):
		return self.samp_rate

	def set_samp_rate(self, samp_rate):
		self.samp_rate = samp_rate
		self.wxgui_scopesink2_0_0.set_sample_rate(self.samp_rate)
		self.gr_sig_source_x_0.set_sampling_freq(self.samp_rate)
		self.wxgui_scopesink2_0.set_sample_rate(self.samp_rate)
		self.low_pass_filter_0.set_taps(firdes.low_pass(2, self.samp_rate, 0.95*self.fc, 1000, firdes.WIN_HAMMING, 6.76))
		self.wxgui_waterfallsink2_0.set_sample_rate(self.samp_rate)

	def get_n_shift(self):
		return self.n_shift

	def set_n_shift(self, n_shift):
		self.n_shift = n_shift
		self.gr_delay_0.set_delay(self.n_shift)
		self._n_shift_slider.set_value(self.n_shift)
		self._n_shift_text_box.set_value(self.n_shift)

	def get_fc(self):
		return self.fc

	def set_fc(self, fc):
		self.fc = fc
		self.gr_sig_source_x_0.set_frequency(self.fc)
		self.low_pass_filter_0.set_taps(firdes.low_pass(2, self.samp_rate, 0.95*self.fc, 1000, firdes.WIN_HAMMING, 6.76))

	def get_N_DATA(self):
		return self.N_DATA

	def set_N_DATA(self, N_DATA):
		self.N_DATA = N_DATA

	def get_Gr(self):
		return self.Gr

	def set_Gr(self, Gr):
		self.Gr = Gr
		self.gr_multiply_const_vxx_0.set_k((self.Gr, ))

if __name__ == '__main__':
	parser = OptionParser(option_class=eng_option, usage="%prog: [options]")
	(options, args) = parser.parse_args()
	tb = top_block()
	tb.Run(True)

