clear all;
close all;
% creat two pulses
hrect = phased.RectangularWaveform('PulseWidth',100e-6,...
    'PRF',1e3,'OutputFormat','Pulses','NumPulses',2);
hrect1 = clone(hrect);
hrect.PulseWidth = 10e-6;
% obtain the shape
y = step(hrect);
y1 = step(hrect1);

totaldur = 2*1/hrect.PRF;
totnumsamp = totaldur*hrect.SampleRate;
t = unigrid(0,1/hrect.SampleRate,totaldur,'[)');
subplot(2,1,1)
plot(t.*1000,y); axis([0 totaldur*1e3 0 1.5]);
title('Two 10-\musec duration pulses (PRF = 1 kHz)');
subplot(2,1,2);
plot(t.*1000,y1); axis([0 totaldur*1e3 0 1.5]);
xlabel('Milliseconds');
title('Two 100-\musec duration pulses (PRF = 1 kHz)');