%% Monostatic pulse radar, adopted from phased-array toolbox demo
clear all;
%% design specifications
pd = 0.9;            % Probability of detection
pfa = 1e-6;          % Probability of false alarm
max_range = 100*1000;    % Maximum unambiguous range
range_res = 2*1000;      % Required range resolution
tgt_rcs = 1;         % Required target radar cross section

%% waveform
prop_speed = physconst('LightSpeed');   % Propagation speed
pulse_bw = prop_speed/(2*range_res);    % Pulse bandwidth
pulse_width = 1/pulse_bw;               % Pulse width
prf = prop_speed/(2*max_range);         % Pulse repetition frequency
fs = 2*pulse_bw;                        % Sampling rate
hwav = phased.RectangularWaveform(...
    'PulseWidth',1/pulse_bw,...
    'PRF',prf,...
    'SampleRate',fs);
% display the wave
y = step(hwav);

figure(100);
totaldur = 1*1/hwav.PRF;
totnumsamp = totaldur*hwav.SampleRate;
t = unigrid(0,1/hwav.SampleRate,totaldur,'[)');
plot(t.*1000,y,'-'); axis([0 totaldur*1e3 0 1.5]);
title('A 33.356 microsec duration pulses (PRF = 29.998 kHz)');
figure(200);
%% noise characteristics
noise_bw = pulse_bw;

hrx = phased.ReceiverPreamp(...
    'Gain',20,...
    'NoiseBandwidth',noise_bw,...
    'NoiseFigure',0,...
    'SampleRate',fs,...
    'EnableInputPort',true);

%% Transmitter
num_pulse_int = 10;
rocsnr([0 3 5],'SignalType','NonfluctuatingNoncoherent',...
    'NumPulses',num_pulse_int);
snr_min = albersheim(pd, pfa, num_pulse_int);
% peak power
tx_gain = 20;

fc = 10e9;
lambda = prop_speed/fc;

peak_power = radareqpow(lambda,max_range,snr_min,pulse_width,...
    'RCS',tgt_rcs,'Gain',tx_gain);

% configure transmitter
htx = phased.Transmitter(...
    'Gain',tx_gain,...
    'PeakPower',peak_power,...
    'InUseOutputPort',true);

%% antenna
hant = phased.IsotropicAntennaElement(...
    'FrequencyRange',[5e9 15e9]);

hantplatform = phased.Platform(...
    'InitialPosition',[0; 0; 0],...
    'Velocity',[0; 0; 0]);
hradiator = phased.Radiator(...
    'Sensor',hant,...
    'OperatingFrequency',fc);

hcollector = phased.Collector(...
    'Sensor',hant,...
    'OperatingFrequency',fc);

%% targets

htarget{1} = phased.RadarTarget(...
    'MeanRCS',1.6,...
    'OperatingFrequency',fc);
htargetplatform{1} = phased.Platform(...
    'InitialPosition',[3*range_res; 0; 0]);

% htarget{2} = phased.RadarTarget(...
%     'MeanRCS',2.2,...
%     'OperatingFrequency',fc);
% htargetplatform{2} = phased.Platform(...
%     'InitialPosition',[4*range_res; 0; 0]);
% 
% htarget{3} = phased.RadarTarget(...
%     'MeanRCS',1.05,...
%     'OperatingFrequency',fc);
% htargetplatform{3} = phased.Platform(...
%     'InitialPosition',[8*range_res; 0; 0]);
%% propagation environment
htargetchannel{1} = phased.FreeSpace(...
    'SampleRate',fs,...
    'TwoWayPropagation',true,...
    'OperatingFrequency',fc);

% htargetchannel{2} = phased.FreeSpace(...
%     'SampleRate',fs,...
%     'TwoWayPropagation',true,...
%     'OperatingFrequency',fc);
% 
% htargetchannel{3} = phased.FreeSpace(...
%     'SampleRate',fs,...
%     'TwoWayPropagation',true,...
%     'OperatingFrequency',fc);

%% signal synthesis

fast_time_grid = unigrid(0,1/fs,1/prf,'[)');
slow_time_grid = (0:num_pulse_int-1)/prf;


hrx.SeedSource = 'Property';
hrx.Seed = 2007;

rx_pulses = zeros(numel(fast_time_grid),num_pulse_int); % pre-allocate

n_tgt=1;
for m = 1:num_pulse_int
    ant_pos = step(hantplatform,1/prf);           % Update antenna position

    x = step(hwav);                               % Generate pulse

    [s, tx_status] = step(htx,x);                 % Transmit pulse


    for n = n_tgt:-1:1                                % For each target

        tgt_pos(:,n) = step(...
            htargetplatform{n},1/prf);            % Update target position

        [tgt_rng(n), tgt_ang(:,n)] = rangeangle(...
            tgt_pos(:,n), ant_pos);               % Calculate range/angle

        tsig(:,n) = step(hradiator,...            % Radiate toward target
            s,tgt_ang(:,n));

        tsig(:,n) = step(htargetchannel{n},...
            tsig(:,n),ant_pos,tgt_pos(:,n));      % Propagate pulse

        rsig(:,n) = step(htarget{n},tsig(:,n));   % Reflect off target
    end

    rsig = step(hcollector,rsig,tgt_ang);         % Collect all echos

    rx_pulses(:,m) = step(hrx,...                 % Receive signal and form
        rsig,~(tx_status>0));                     % data matrix
end

%% detection
npower = noisepow(noise_bw,hrx.NoiseFigure,hrx.ReferenceTemperature);
threshold = npower * db2pow(npwgnthresh(pfa,num_pulse_int,'noncoherent'));

num_pulse_plot = 2;
figure(1);
rangedemoplotpulse(rx_pulses,threshold,...
    fast_time_grid,slow_time_grid,num_pulse_plot);


%% matched filtering
matchingcoeff = getMatchedFilter(hwav);
hmf = phased.MatchedFilter(...
    'Coefficients',matchingcoeff,...
    'GainOutputPort',true);
[rx_pulses, mfgain] = step(hmf,rx_pulses);

%delay compensation of mf
matchingdelay = size(matchingcoeff,1)-1;
rx_pulses = buffer(rx_pulses(matchingdelay+1:end),size(rx_pulses,1));
threshold = threshold * db2pow(mfgain);
figure(2);
rangedemoplotpulse(rx_pulses,threshold,...
    fast_time_grid,slow_time_grid,num_pulse_plot);

%% range-gates

range_gates = prop_speed*fast_time_grid/2;

htvg = phased.TimeVaryingGain(...
    'RangeLoss',2*fspl(range_gates,lambda),...
    'ReferenceLoss',2*fspl(max_range,lambda));

rx_pulses = step(htvg,rx_pulses);

figure(3);
rangedemoplotpulse(rx_pulses,threshold,...
    fast_time_grid,slow_time_grid,num_pulse_plot);


