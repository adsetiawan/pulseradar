clear all; close all;
%%
samp_rate=1e4;
dt=1/samp_rate;
readsize=20*1000;

%% Tx
fidt00=fopen('data/Tx/d0.dat');
fidt10=fopen('data/Tx/d1.dat');
fidt20=fopen('data/Tx/d2.dat');
fidt30=fopen('data/Tx/d3.dat');
fidt40=fopen('data/Tx/d4.dat');
fidt50=fopen('data/Tx/d5.dat');

[yt00, count] = fread (fidt00, readsize, 'float'); 
[yt10, count] = fread (fidt10, readsize, 'float'); 
[yt20, count] = fread (fidt20, readsize, 'float'); 
[yt30, count] = fread (fidt30, readsize, 'float'); 
[yt40, count] = fread (fidt40, readsize, 'float'); 
[yt50, count] = fread (fidt50, readsize, 'float'); 
fclose(fidt00);
fclose(fidt10);
fclose(fidt20);
fclose(fidt30);
fclose(fidt40);
fclose(fidt50);
%% Rx
fidr00=fopen('data/Rx/d0.dat');
fidr10=fopen('data/Rx/d1.dat');
fidr20=fopen('data/Rx/d2.dat');
fidr30=fopen('data/Rx/d3.dat');
fidr40=fopen('data/Rx/d4.dat');
fidr50=fopen('data/Rx/d5.dat');
[yr00, count] = fread (fidr00, readsize, 'float'); 
[yr10, count] = fread (fidr10, readsize, 'float'); 
[yr20, count] = fread (fidr20, readsize, 'float'); 
[yr30, count] = fread (fidr30, readsize, 'float'); 
[yr40, count] = fread (fidr40, readsize, 'float'); 
[yr50, count] = fread (fidr50, readsize, 'float'); 

fclose(fidr00);
fclose(fidr10);
fclose(fidr20);
fclose(fidr30);
fclose(fidr40);
fclose(fidr50);
%% display
s=1;
e=1*10000;
ytt00=yt00(s:e);
ytt10=yt10(s:e);
ytt20=yt20(s:e);
ytt30=yt30(s:e);
ytt40=yt40(s:e);
ytt50=yt50(s:e);

ytr00=yr00(s:e);
ytr10=yr10(s:e);
ytr20=yr20(s:e);
ytr30=yr30(s:e);
ytr40=yr40(s:e);
ytr50=yr50(s:e);

t=dt*(1:length(ytt00));
figure(2);
subplot(6,1,1); plot(t,ytt00,'b',t,ytr00,'r');
subplot(6,1,2); plot(t,ytt10,'b',t,ytr10,'r');
subplot(6,1,3); plot(t,ytt20,'b',t,ytr20,'r');
subplot(6,1,4); plot(t,ytt30,'b',t,ytr30,'r');
subplot(6,1,5); plot(t,ytt40,'b',t,ytr40,'r');
subplot(6,1,6); plot(t,ytt50,'b',t,ytr50,'r');