clear all;
%% natural parameters
c=3e8; % speed of light

%% oversampling factor 
f_oversamp=20; % 10 Ms

%% design specifications
max_range = 100*1000;    % Maximum unambiguous range
range_res = 5*1000;      % Required range resolution

%% waveform
pulse_bw = c/(2*range_res);    % Pulse bandwidth
pulse_width = 1/pulse_bw;      % Pulse width
prf = c/(2*max_range);         % Pulse repetition frequency
fs = f_oversamp*(2*pulse_bw);                        % Sampling rate

totaldur = 1/prf;
ndur=round(totaldur*fs);
totnumsamp = totaldur*fs;
dt=1/fs;
t = dt*transpose(0:ndur-1);
sp=zeros(length(t),1);
sp(1:round(fs*pulse_width))=1;

%% carrier 
fc=4*pulse_bw;  % 300 KHz
sc=sin(2*pi*fc*t);
%
%% display parameters
disp(sprintf('fs=%3.3e, Ru=%3.3e, dr=%3.3e, bw=%3.3e',fs, max_range, range_res, pulse_bw));
disp(sprintf('tau=%3.3e, PRF=%3.3e, fc=%3.3e',pulse_width, prf, fc));


figure(1);
subplot(311);plot(t,sp);
subplot(312);plot(t,sc);
subplot(313);plot(t,sp.*sc);
