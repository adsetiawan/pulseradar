#!/usr/bin/env python
##################################################
# Gnuradio Python Flow Graph
# Title: Pulse Radar Simulation
# Author: Andriyan Suksmono
# Generated: Tue Apr 30 13:38:37 2013
##################################################

from gnuradio import eng_notation
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.gr import firdes
from gnuradio.wxgui import scopesink2
from grc_gnuradio import wxgui as grc_wxgui
from optparse import OptionParser
import wx

class pulse_radar(grc_wxgui.top_block_gui):

	def __init__(self):
		grc_wxgui.top_block_gui.__init__(self, title="Pulse Radar Simulation")
		_icon_path = "/usr/share/icons/hicolor/32x32/apps/gnuradio-grc.png"
		self.SetIcon(wx.Icon(_icon_path, wx.BITMAP_TYPE_ANY))

		##################################################
		# Variables
		##################################################
		self.samp_rate = samp_rate = 100000

		##################################################
		# Blocks
		##################################################
		self.notebook_0 = self.notebook_0 = wx.Notebook(self.GetWin(), style=wx.NB_TOP)
		self.notebook_0.AddPage(grc_wxgui.Panel(self.notebook_0), "Clock")
		self.notebook_0.AddPage(grc_wxgui.Panel(self.notebook_0), "IF")
		self.notebook_0.AddPage(grc_wxgui.Panel(self.notebook_0), "Tx")
		self.notebook_0.AddPage(grc_wxgui.Panel(self.notebook_0), "Rx")
		self.Add(self.notebook_0)
		self.wxgui_scopesink2_0_2_0 = scopesink2.scope_sink_f(
			self.notebook_0.GetPage(0).GetWin(),
			title="Scope Plot",
			sample_rate=samp_rate,
			v_scale=0,
			v_offset=0,
			t_scale=0,
			ac_couple=False,
			xy_mode=False,
			num_inputs=2,
			trig_mode=gr.gr_TRIG_MODE_AUTO,
			y_axis_label="Counts",
		)
		self.notebook_0.GetPage(0).Add(self.wxgui_scopesink2_0_2_0.win)
		self.wxgui_scopesink2_0_2 = scopesink2.scope_sink_f(
			self.notebook_0.GetPage(1).GetWin(),
			title="Scope Plot",
			sample_rate=samp_rate,
			v_scale=0,
			v_offset=0,
			t_scale=0,
			ac_couple=False,
			xy_mode=False,
			num_inputs=1,
			trig_mode=gr.gr_TRIG_MODE_AUTO,
			y_axis_label="Counts",
		)
		self.notebook_0.GetPage(1).Add(self.wxgui_scopesink2_0_2.win)
		self.wxgui_scopesink2_0_0 = scopesink2.scope_sink_f(
			self.notebook_0.GetPage(3).GetWin(),
			title="Scope Plot",
			sample_rate=samp_rate,
			v_scale=0,
			v_offset=0,
			t_scale=0,
			ac_couple=False,
			xy_mode=False,
			num_inputs=1,
			trig_mode=gr.gr_TRIG_MODE_AUTO,
			y_axis_label="Counts",
		)
		self.notebook_0.GetPage(3).Add(self.wxgui_scopesink2_0_0.win)
		self.wxgui_scopesink2_0 = scopesink2.scope_sink_f(
			self.notebook_0.GetPage(2).GetWin(),
			title="Scope Plot",
			sample_rate=samp_rate,
			v_scale=0,
			v_offset=0,
			t_scale=0,
			ac_couple=False,
			xy_mode=False,
			num_inputs=1,
			trig_mode=gr.gr_TRIG_MODE_AUTO,
			y_axis_label="Counts",
		)
		self.notebook_0.GetPage(2).Add(self.wxgui_scopesink2_0.win)
		self.gr_vector_source_x_0 = gr.vector_source_f((0,1), True, 1)
		self.gr_sig_source_x_0 = gr.sig_source_f(samp_rate, gr.GR_COS_WAVE, 100, 1, 0)
		self.gr_repeat_0 = gr.repeat(gr.sizeof_float*1, 10000)
		self.gr_multiply_xx_0_0 = gr.multiply_vff(1)
		self.gr_multiply_xx_0 = gr.multiply_vff(1)
		self.gr_delay_1 = gr.delay(gr.sizeof_float*1, 3000)
		self.gr_delay_0 = gr.delay(gr.sizeof_float*1, 5000)

		##################################################
		# Connections
		##################################################
		self.connect((self.gr_sig_source_x_0, 0), (self.gr_multiply_xx_0, 0))
		self.connect((self.gr_repeat_0, 0), (self.gr_multiply_xx_0, 1))
		self.connect((self.gr_vector_source_x_0, 0), (self.gr_repeat_0, 0))
		self.connect((self.gr_multiply_xx_0, 0), (self.wxgui_scopesink2_0, 0))
		self.connect((self.gr_multiply_xx_0, 0), (self.gr_delay_0, 0))
		self.connect((self.gr_delay_0, 0), (self.gr_multiply_xx_0_0, 0))
		self.connect((self.gr_repeat_0, 0), (self.gr_multiply_xx_0_0, 1))
		self.connect((self.gr_multiply_xx_0_0, 0), (self.wxgui_scopesink2_0_0, 0))
		self.connect((self.gr_sig_source_x_0, 0), (self.wxgui_scopesink2_0_2, 0))
		self.connect((self.gr_repeat_0, 0), (self.wxgui_scopesink2_0_2_0, 0))
		self.connect((self.gr_repeat_0, 0), (self.gr_delay_1, 0))
		self.connect((self.gr_delay_1, 0), (self.wxgui_scopesink2_0_2_0, 1))

	def get_samp_rate(self):
		return self.samp_rate

	def set_samp_rate(self, samp_rate):
		self.samp_rate = samp_rate
		self.wxgui_scopesink2_0_2.set_sample_rate(self.samp_rate)
		self.gr_sig_source_x_0.set_sampling_freq(self.samp_rate)
		self.wxgui_scopesink2_0.set_sample_rate(self.samp_rate)
		self.wxgui_scopesink2_0_0.set_sample_rate(self.samp_rate)
		self.wxgui_scopesink2_0_2_0.set_sample_rate(self.samp_rate)

if __name__ == '__main__':
	parser = OptionParser(option_class=eng_option, usage="%prog: [options]")
	(options, args) = parser.parse_args()
	tb = pulse_radar()
	tb.Run(True)

