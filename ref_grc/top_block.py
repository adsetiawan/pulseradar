#!/usr/bin/env python
##################################################
# Gnuradio Python Flow Graph
# Title: Top Block
# Generated: Thu May  2 10:35:48 2013
##################################################

from gnuradio import eng_notation
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.gr import firdes
from gnuradio.wxgui import scopesink2
from grc_gnuradio import wxgui as grc_wxgui
from optparse import OptionParser
import wx

class top_block(grc_wxgui.top_block_gui):

	def __init__(self):
		grc_wxgui.top_block_gui.__init__(self, title="Top Block")
		_icon_path = "/usr/share/icons/hicolor/32x32/apps/gnuradio-grc.png"
		self.SetIcon(wx.Icon(_icon_path, wx.BITMAP_TYPE_ANY))

		##################################################
		# Variables
		##################################################
		self.range_res = range_res = 5*1000
		self.prop_speed = prop_speed = 300000000
		self.samp_rate = samp_rate = 1200*1000
		self.pulse_bw = pulse_bw = prop_speed/(2*range_res)
		self.max_range = max_range = 100000
		self.pulse_width = pulse_width = 1/pulse_bw
		self.fc = fc = 120*1000
		self.dt_tgt2 = dt_tgt2 = 4*int(2*range_res*samp_rate/prop_speed)
		self.dt_tgt1 = dt_tgt1 = 1*int(2*range_res*samp_rate/prop_speed)
		self.PRF = PRF = prop_speed/(2*max_range)

		##################################################
		# Blocks
		##################################################
		self.notebook_0 = self.notebook_0 = wx.Notebook(self.GetWin(), style=wx.NB_TOP)
		self.notebook_0.AddPage(grc_wxgui.Panel(self.notebook_0), "carrier")
		self.notebook_0.AddPage(grc_wxgui.Panel(self.notebook_0), "pulse")
		self.notebook_0.AddPage(grc_wxgui.Panel(self.notebook_0), "modulated")
		self.notebook_0.AddPage(grc_wxgui.Panel(self.notebook_0), "A-scan")
		self.Add(self.notebook_0)
		self.wxgui_scopesink2_0_1 = scopesink2.scope_sink_f(
			self.notebook_0.GetPage(2).GetWin(),
			title="Scope Plot",
			sample_rate=samp_rate,
			v_scale=0,
			v_offset=0,
			t_scale=0,
			ac_couple=False,
			xy_mode=False,
			num_inputs=1,
			trig_mode=gr.gr_TRIG_MODE_AUTO,
			y_axis_label="Counts",
		)
		self.notebook_0.GetPage(2).Add(self.wxgui_scopesink2_0_1.win)
		self.wxgui_scopesink2_0_0_1 = scopesink2.scope_sink_c(
			self.notebook_0.GetPage(3).GetWin(),
			title="Scope Plot",
			sample_rate=samp_rate,
			v_scale=0,
			v_offset=0,
			t_scale=0,
			ac_couple=False,
			xy_mode=False,
			num_inputs=1,
			trig_mode=gr.gr_TRIG_MODE_AUTO,
			y_axis_label="Counts",
		)
		self.notebook_0.GetPage(3).Add(self.wxgui_scopesink2_0_0_1.win)
		self.wxgui_scopesink2_0_0 = scopesink2.scope_sink_f(
			self.notebook_0.GetPage(1).GetWin(),
			title="Scope Plot",
			sample_rate=samp_rate,
			v_scale=0,
			v_offset=0,
			t_scale=0,
			ac_couple=False,
			xy_mode=False,
			num_inputs=1,
			trig_mode=gr.gr_TRIG_MODE_AUTO,
			y_axis_label="Counts",
		)
		self.notebook_0.GetPage(1).Add(self.wxgui_scopesink2_0_0.win)
		self.wxgui_scopesink2_0 = scopesink2.scope_sink_f(
			self.notebook_0.GetPage(0).GetWin(),
			title="Scope Plot",
			sample_rate=samp_rate,
			v_scale=0,
			v_offset=0,
			t_scale=0,
			ac_couple=False,
			xy_mode=False,
			num_inputs=1,
			trig_mode=gr.gr_TRIG_MODE_AUTO,
			y_axis_label="Counts",
		)
		self.notebook_0.GetPage(0).Add(self.wxgui_scopesink2_0.win)
		self.low_pass_filter_0 = gr.fir_filter_fff(1, firdes.low_pass(
			1, samp_rate, fc-fc/200, fc/100, firdes.WIN_HAMMING, 6.76))
		self.gr_vector_source_x_0 = gr.vector_source_f((1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0), True, 1)
		self.gr_sig_source_x_0 = gr.sig_source_f(samp_rate, gr.GR_COS_WAVE, fc, 1, 0)
		self.gr_repeat_0 = gr.repeat(gr.sizeof_float*1, 40)
		self.gr_multiply_xx_1 = gr.multiply_vff(1)
		self.gr_multiply_xx_0 = gr.multiply_vff(1)
		self.gr_float_to_complex_0 = gr.float_to_complex(1)
		self.gr_delay_0_0 = gr.delay(gr.sizeof_int*1, dt_tgt2)
		self.gr_delay_0 = gr.delay(gr.sizeof_int*1, dt_tgt1)
		self.gr_add_xx_0 = gr.add_vff(1)

		##################################################
		# Connections
		##################################################
		self.connect((self.gr_vector_source_x_0, 0), (self.gr_repeat_0, 0))
		self.connect((self.gr_sig_source_x_0, 0), (self.gr_multiply_xx_0, 0))
		self.connect((self.gr_sig_source_x_0, 0), (self.wxgui_scopesink2_0, 0))
		self.connect((self.gr_repeat_0, 0), (self.wxgui_scopesink2_0_0, 0))
		self.connect((self.gr_multiply_xx_0, 0), (self.wxgui_scopesink2_0_1, 0))
		self.connect((self.gr_repeat_0, 0), (self.gr_multiply_xx_0, 1))
		self.connect((self.gr_multiply_xx_0, 0), (self.gr_delay_0, 0))
		self.connect((self.gr_sig_source_x_0, 0), (self.gr_multiply_xx_1, 0))
		self.connect((self.gr_multiply_xx_1, 0), (self.low_pass_filter_0, 0))
		self.connect((self.gr_repeat_0, 0), (self.gr_float_to_complex_0, 1))
		self.connect((self.low_pass_filter_0, 0), (self.gr_float_to_complex_0, 0))
		self.connect((self.gr_float_to_complex_0, 0), (self.wxgui_scopesink2_0_0_1, 0))
		self.connect((self.gr_multiply_xx_0, 0), (self.gr_delay_0_0, 0))
		self.connect((self.gr_delay_0, 0), (self.gr_add_xx_0, 0))
		self.connect((self.gr_delay_0_0, 0), (self.gr_add_xx_0, 1))
		self.connect((self.gr_add_xx_0, 0), (self.gr_multiply_xx_1, 1))

	def get_range_res(self):
		return self.range_res

	def set_range_res(self, range_res):
		self.range_res = range_res
		self.set_pulse_bw(self.prop_speed/(2*self.range_res))
		self.set_dt_tgt1(1*int(2*self.range_res*self.samp_rate/self.prop_speed))
		self.set_dt_tgt2(4*int(2*self.range_res*self.samp_rate/self.prop_speed))

	def get_prop_speed(self):
		return self.prop_speed

	def set_prop_speed(self, prop_speed):
		self.prop_speed = prop_speed
		self.set_pulse_bw(self.prop_speed/(2*self.range_res))
		self.set_PRF(self.prop_speed/(2*self.max_range))
		self.set_dt_tgt1(1*int(2*self.range_res*self.samp_rate/self.prop_speed))
		self.set_dt_tgt2(4*int(2*self.range_res*self.samp_rate/self.prop_speed))

	def get_samp_rate(self):
		return self.samp_rate

	def set_samp_rate(self, samp_rate):
		self.samp_rate = samp_rate
		self.wxgui_scopesink2_0.set_sample_rate(self.samp_rate)
		self.wxgui_scopesink2_0_1.set_sample_rate(self.samp_rate)
		self.gr_sig_source_x_0.set_sampling_freq(self.samp_rate)
		self.wxgui_scopesink2_0_0.set_sample_rate(self.samp_rate)
		self.wxgui_scopesink2_0_0_1.set_sample_rate(self.samp_rate)
		self.set_dt_tgt1(1*int(2*self.range_res*self.samp_rate/self.prop_speed))
		self.set_dt_tgt2(4*int(2*self.range_res*self.samp_rate/self.prop_speed))
		self.low_pass_filter_0.set_taps(firdes.low_pass(1, self.samp_rate, self.fc-self.fc/200, self.fc/100, firdes.WIN_HAMMING, 6.76))

	def get_pulse_bw(self):
		return self.pulse_bw

	def set_pulse_bw(self, pulse_bw):
		self.pulse_bw = pulse_bw
		self.set_pulse_width(1/self.pulse_bw)

	def get_max_range(self):
		return self.max_range

	def set_max_range(self, max_range):
		self.max_range = max_range
		self.set_PRF(self.prop_speed/(2*self.max_range))

	def get_pulse_width(self):
		return self.pulse_width

	def set_pulse_width(self, pulse_width):
		self.pulse_width = pulse_width

	def get_fc(self):
		return self.fc

	def set_fc(self, fc):
		self.fc = fc
		self.gr_sig_source_x_0.set_frequency(self.fc)
		self.low_pass_filter_0.set_taps(firdes.low_pass(1, self.samp_rate, self.fc-self.fc/200, self.fc/100, firdes.WIN_HAMMING, 6.76))

	def get_dt_tgt2(self):
		return self.dt_tgt2

	def set_dt_tgt2(self, dt_tgt2):
		self.dt_tgt2 = dt_tgt2
		self.gr_delay_0_0.set_delay(self.dt_tgt2)

	def get_dt_tgt1(self):
		return self.dt_tgt1

	def set_dt_tgt1(self, dt_tgt1):
		self.dt_tgt1 = dt_tgt1
		self.gr_delay_0.set_delay(self.dt_tgt1)

	def get_PRF(self):
		return self.PRF

	def set_PRF(self, PRF):
		self.PRF = PRF

if __name__ == '__main__':
	parser = OptionParser(option_class=eng_option, usage="%prog: [options]")
	(options, args) = parser.parse_args()
	tb = top_block()
	tb.Run(True)

