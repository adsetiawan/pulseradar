#!/usr/bin/env python
##################################################
# Gnuradio Python Flow Graph
# Title: Pulsed Radar8M
# Generated: Tue Apr 30 13:45:24 2013
##################################################

from gnuradio import eng_notation
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.gr import firdes
from gnuradio.wxgui import scopesink2
from grc_gnuradio import wxgui as grc_wxgui
from optparse import OptionParser
import wx

class pulsed_radar8M(grc_wxgui.top_block_gui):

	def __init__(self):
		grc_wxgui.top_block_gui.__init__(self, title="Pulsed Radar8M")
		_icon_path = "/usr/share/icons/hicolor/32x32/apps/gnuradio-grc.png"
		self.SetIcon(wx.Icon(_icon_path, wx.BITMAP_TYPE_ANY))

		##################################################
		# Variables
		##################################################
		self.samp_rate = samp_rate = 10000000
		self.N_REPEAT = N_REPEAT = samp_rate/100000
		self.N_DELAY = N_DELAY = 60

		##################################################
		# Blocks
		##################################################
		self.notebook_0 = self.notebook_0 = wx.Notebook(self.GetWin(), style=wx.NB_TOP)
		self.notebook_0.AddPage(grc_wxgui.Panel(self.notebook_0), "input-signals")
		self.notebook_0.AddPage(grc_wxgui.Panel(self.notebook_0), "Tx-signal")
		self.notebook_0.AddPage(grc_wxgui.Panel(self.notebook_0), "Rx-signal")
		self.Add(self.notebook_0)
		self.wxgui_scopesink2_0_0_0 = scopesink2.scope_sink_f(
			self.notebook_0.GetPage(2).GetWin(),
			title="Scope Plot",
			sample_rate=samp_rate,
			v_scale=0,
			v_offset=0,
			t_scale=0,
			ac_couple=False,
			xy_mode=False,
			num_inputs=2,
			trig_mode=gr.gr_TRIG_MODE_AUTO,
			y_axis_label="Counts",
		)
		self.notebook_0.GetPage(2).Add(self.wxgui_scopesink2_0_0_0.win)
		self.wxgui_scopesink2_0_0 = scopesink2.scope_sink_f(
			self.notebook_0.GetPage(1).GetWin(),
			title="Scope Plot",
			sample_rate=samp_rate,
			v_scale=0,
			v_offset=0,
			t_scale=0,
			ac_couple=False,
			xy_mode=False,
			num_inputs=1,
			trig_mode=gr.gr_TRIG_MODE_AUTO,
			y_axis_label="Counts",
		)
		self.notebook_0.GetPage(1).Add(self.wxgui_scopesink2_0_0.win)
		self.wxgui_scopesink2_0 = scopesink2.scope_sink_f(
			self.notebook_0.GetPage(0).GetWin(),
			title="Scope Plot",
			sample_rate=samp_rate,
			v_scale=0,
			v_offset=0,
			t_scale=0,
			ac_couple=False,
			xy_mode=False,
			num_inputs=2,
			trig_mode=gr.gr_TRIG_MODE_AUTO,
			y_axis_label="Counts",
		)
		self.notebook_0.GetPage(0).Add(self.wxgui_scopesink2_0.win)
		self.target_at_9km = gr.delay(gr.sizeof_float*1, N_DELAY)
		self.sf2 = gr.sig_source_f(samp_rate, gr.GR_COS_WAVE, 100000, 1, 0)
		self.sf1 = gr.sig_source_f(samp_rate, gr.GR_COS_WAVE, 700000, 1, 0)
		self.gr_vector_source_x_0 = gr.vector_source_f((1,0), True, 1)
		self.gr_repeat_0 = gr.repeat(gr.sizeof_float*1, N_REPEAT)
		self.gr_multiply_xx_1 = gr.multiply_vff(1)
		self.gr_multiply_xx_0_0_0 = gr.multiply_vff(1)
		self.gr_multiply_xx_0_0 = gr.multiply_vff(1)
		self.gr_multiply_xx_0 = gr.multiply_vff(1)
		self.band_pass_filter_0 = gr.interp_fir_filter_fff(1, firdes.band_pass(
			1, samp_rate, 780000, 812000, 10000, firdes.WIN_RECTANGULAR, 6.76))
		self.Amplifier = gr.sig_source_f(0, gr.GR_CONST_WAVE, 0, 0, 2)

		##################################################
		# Connections
		##################################################
		self.connect((self.sf1, 0), (self.wxgui_scopesink2_0, 0))
		self.connect((self.gr_vector_source_x_0, 0), (self.gr_repeat_0, 0))
		self.connect((self.gr_repeat_0, 0), (self.wxgui_scopesink2_0, 1))
		self.connect((self.gr_repeat_0, 0), (self.gr_multiply_xx_0, 1))
		self.connect((self.gr_multiply_xx_0, 0), (self.wxgui_scopesink2_0_0, 0))
		self.connect((self.gr_multiply_xx_0, 0), (self.target_at_9km, 0))
		self.connect((self.sf1, 0), (self.gr_multiply_xx_0_0, 0))
		self.connect((self.sf2, 0), (self.gr_multiply_xx_0_0, 1))
		self.connect((self.gr_multiply_xx_0_0, 0), (self.band_pass_filter_0, 0))
		self.connect((self.band_pass_filter_0, 0), (self.gr_multiply_xx_0, 0))
		self.connect((self.Amplifier, 0), (self.gr_multiply_xx_1, 0))
		self.connect((self.gr_multiply_xx_0_0_0, 0), (self.gr_multiply_xx_1, 1))
		self.connect((self.sf1, 0), (self.gr_multiply_xx_0_0_0, 0))
		self.connect((self.target_at_9km, 0), (self.gr_multiply_xx_0_0_0, 1))
		self.connect((self.gr_multiply_xx_1, 0), (self.wxgui_scopesink2_0_0_0, 0))
		self.connect((self.sf2, 0), (self.wxgui_scopesink2_0_0_0, 1))

	def get_samp_rate(self):
		return self.samp_rate

	def set_samp_rate(self, samp_rate):
		self.samp_rate = samp_rate
		self.wxgui_scopesink2_0.set_sample_rate(self.samp_rate)
		self.wxgui_scopesink2_0_0.set_sample_rate(self.samp_rate)
		self.sf1.set_sampling_freq(self.samp_rate)
		self.sf2.set_sampling_freq(self.samp_rate)
		self.band_pass_filter_0.set_taps(firdes.band_pass(1, self.samp_rate, 780000, 812000, 10000, firdes.WIN_RECTANGULAR, 6.76))
		self.set_N_REPEAT(self.samp_rate/100000)
		self.wxgui_scopesink2_0_0_0.set_sample_rate(self.samp_rate)

	def get_N_REPEAT(self):
		return self.N_REPEAT

	def set_N_REPEAT(self, N_REPEAT):
		self.N_REPEAT = N_REPEAT

	def get_N_DELAY(self):
		return self.N_DELAY

	def set_N_DELAY(self, N_DELAY):
		self.N_DELAY = N_DELAY
		self.target_at_9km.set_delay(self.N_DELAY)

if __name__ == '__main__':
	parser = OptionParser(option_class=eng_option, usage="%prog: [options]")
	(options, args) = parser.parse_args()
	tb = pulsed_radar8M()
	tb.Run(True)

