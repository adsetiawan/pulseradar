#!/usr/bin/env python
##################################################
# Gnuradio Python Flow Graph
# Title: Simulate a pulsed radar design as in Matlab-phased-array toolbox
# Author: Andriyan Suksmono
# Generated: Wed May  1 16:09:36 2013
##################################################

from gnuradio import eng_notation
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.gr import firdes
from gnuradio.wxgui import scopesink2
from grc_gnuradio import wxgui as grc_wxgui
from optparse import OptionParser
import wx

class pulse_radar5kmrange(grc_wxgui.top_block_gui):

	def __init__(self):
		grc_wxgui.top_block_gui.__init__(self, title="Simulate a pulsed radar design as in Matlab-phased-array toolbox")
		_icon_path = "/usr/share/icons/hicolor/32x32/apps/gnuradio-grc.png"
		self.SetIcon(wx.Icon(_icon_path, wx.BITMAP_TYPE_ANY))

		##################################################
		# Variables
		##################################################
		self.range_res = range_res = 2*1000
		self.prop_speed = prop_speed = 299792458
		self.samp_rate = samp_rate = 10*1000000
		self.pulse_bw = pulse_bw = prop_speed/(2*range_res)
		self.max_range = max_range = 100000
		self.pulse_width = pulse_width = 1/pulse_bw
		self.fc = fc = 1*1000000
		self.dt_tgt1 = dt_tgt1 = int(3*2*range_res*samp_rate/prop_speed)
		self.PRF = PRF = prop_speed/(2*max_range)

		##################################################
		# Blocks
		##################################################
		self.notebook_0 = self.notebook_0 = wx.Notebook(self.GetWin(), style=wx.NB_TOP)
		self.notebook_0.AddPage(grc_wxgui.Panel(self.notebook_0), "signals")
		self.notebook_0.AddPage(grc_wxgui.Panel(self.notebook_0), "Tx")
		self.notebook_0.AddPage(grc_wxgui.Panel(self.notebook_0), "Rx")
		self.Add(self.notebook_0)
		self.wxgui_scopesink2_0_2 = scopesink2.scope_sink_f(
			self.notebook_0.GetPage(0).GetWin(),
			title="Scope Plot",
			sample_rate=samp_rate,
			v_scale=0,
			v_offset=0,
			t_scale=0,
			ac_couple=False,
			xy_mode=False,
			num_inputs=2,
			trig_mode=gr.gr_TRIG_MODE_AUTO,
			y_axis_label="Counts",
		)
		self.notebook_0.GetPage(0).Add(self.wxgui_scopesink2_0_2.win)
		self.wxgui_scopesink2_0_0 = scopesink2.scope_sink_f(
			self.notebook_0.GetPage(2).GetWin(),
			title="Scope Plot",
			sample_rate=samp_rate,
			v_scale=0,
			v_offset=0,
			t_scale=0,
			ac_couple=False,
			xy_mode=False,
			num_inputs=2,
			trig_mode=gr.gr_TRIG_MODE_AUTO,
			y_axis_label="Counts",
		)
		self.notebook_0.GetPage(2).Add(self.wxgui_scopesink2_0_0.win)
		self.wxgui_scopesink2_0 = scopesink2.scope_sink_f(
			self.notebook_0.GetPage(1).GetWin(),
			title="Scope Plot",
			sample_rate=samp_rate,
			v_scale=0,
			v_offset=0,
			t_scale=0,
			ac_couple=False,
			xy_mode=False,
			num_inputs=1,
			trig_mode=gr.gr_TRIG_MODE_AUTO,
			y_axis_label="Counts",
		)
		self.notebook_0.GetPage(1).Add(self.wxgui_scopesink2_0.win)
		self.low_pass_filter_0 = gr.fir_filter_fff(1, firdes.low_pass(
			2, samp_rate, fc-fc/200, fc/100, firdes.WIN_HAMMING, 6.76))
		self.gr_threshold_ff_0 = gr.threshold_ff(samp_rate/pulse_bw, samp_rate/pulse_bw , 0)
		self.gr_sig_source_x_1 = gr.sig_source_f(samp_rate, gr.GR_SAW_WAVE, PRF, samp_rate/1000, 0)
		self.gr_sig_source_x_0 = gr.sig_source_f(samp_rate, gr.GR_COS_WAVE, fc, 1, 0)
		self.gr_multiply_xx_1 = gr.multiply_vff(1)
		self.gr_multiply_xx_0_0 = gr.multiply_vff(1)
		self.gr_multiply_xx_0 = gr.multiply_vff(1)
		self.gr_delay_0 = gr.delay(gr.sizeof_int*1, dt_tgt1)
		self.gr_add_xx_0 = gr.add_vff(1)
		self.const_source_x_0_0_0 = gr.sig_source_f(0, gr.GR_CONST_WAVE, 0, 0, -1)
		self.const_source_x_0_0 = gr.sig_source_f(0, gr.GR_CONST_WAVE, 0, 0, -1)

		##################################################
		# Connections
		##################################################
		self.connect((self.gr_multiply_xx_0, 0), (self.wxgui_scopesink2_0, 0))
		self.connect((self.gr_sig_source_x_1, 0), (self.gr_threshold_ff_0, 0))
		self.connect((self.gr_sig_source_x_0, 0), (self.gr_multiply_xx_0, 0))
		self.connect((self.const_source_x_0_0_0, 0), (self.gr_multiply_xx_0_0, 1))
		self.connect((self.gr_threshold_ff_0, 0), (self.gr_add_xx_0, 0))
		self.connect((self.gr_sig_source_x_0, 0), (self.wxgui_scopesink2_0_2, 0))
		self.connect((self.gr_multiply_xx_0, 0), (self.gr_delay_0, 0))
		self.connect((self.gr_sig_source_x_0, 0), (self.gr_multiply_xx_1, 1))
		self.connect((self.gr_delay_0, 0), (self.gr_multiply_xx_1, 0))
		self.connect((self.gr_multiply_xx_1, 0), (self.wxgui_scopesink2_0_0, 0))
		self.connect((self.low_pass_filter_0, 0), (self.wxgui_scopesink2_0_0, 1))
		self.connect((self.gr_multiply_xx_1, 0), (self.low_pass_filter_0, 0))
		self.connect((self.gr_multiply_xx_0_0, 0), (self.wxgui_scopesink2_0_2, 1))
		self.connect((self.gr_multiply_xx_0_0, 0), (self.gr_multiply_xx_0, 1))
		self.connect((self.const_source_x_0_0, 0), (self.gr_add_xx_0, 1))
		self.connect((self.gr_add_xx_0, 0), (self.gr_multiply_xx_0_0, 0))

	def get_range_res(self):
		return self.range_res

	def set_range_res(self, range_res):
		self.range_res = range_res
		self.set_pulse_bw(self.prop_speed/(2*self.range_res))
		self.set_dt_tgt1(int(3*2*self.range_res*self.samp_rate/self.prop_speed))

	def get_prop_speed(self):
		return self.prop_speed

	def set_prop_speed(self, prop_speed):
		self.prop_speed = prop_speed
		self.set_pulse_bw(self.prop_speed/(2*self.range_res))
		self.set_PRF(self.prop_speed/(2*self.max_range))
		self.set_dt_tgt1(int(3*2*self.range_res*self.samp_rate/self.prop_speed))

	def get_samp_rate(self):
		return self.samp_rate

	def set_samp_rate(self, samp_rate):
		self.samp_rate = samp_rate
		self.gr_sig_source_x_0.set_sampling_freq(self.samp_rate)
		self.wxgui_scopesink2_0_2.set_sample_rate(self.samp_rate)
		self.wxgui_scopesink2_0.set_sample_rate(self.samp_rate)
		self.gr_threshold_ff_0.set_hi(self.samp_rate/self.pulse_bw )
		self.gr_threshold_ff_0.set_lo(self.samp_rate/self.pulse_bw)
		self.low_pass_filter_0.set_taps(firdes.low_pass(2, self.samp_rate, self.fc-self.fc/200, self.fc/100, firdes.WIN_HAMMING, 6.76))
		self.wxgui_scopesink2_0_0.set_sample_rate(self.samp_rate)
		self.set_dt_tgt1(int(3*2*self.range_res*self.samp_rate/self.prop_speed))
		self.gr_sig_source_x_1.set_sampling_freq(self.samp_rate)
		self.gr_sig_source_x_1.set_amplitude(self.samp_rate/1000)

	def get_pulse_bw(self):
		return self.pulse_bw

	def set_pulse_bw(self, pulse_bw):
		self.pulse_bw = pulse_bw
		self.gr_threshold_ff_0.set_hi(self.samp_rate/self.pulse_bw )
		self.gr_threshold_ff_0.set_lo(self.samp_rate/self.pulse_bw)
		self.set_pulse_width(1/self.pulse_bw)

	def get_max_range(self):
		return self.max_range

	def set_max_range(self, max_range):
		self.max_range = max_range
		self.set_PRF(self.prop_speed/(2*self.max_range))

	def get_pulse_width(self):
		return self.pulse_width

	def set_pulse_width(self, pulse_width):
		self.pulse_width = pulse_width

	def get_fc(self):
		return self.fc

	def set_fc(self, fc):
		self.fc = fc
		self.gr_sig_source_x_0.set_frequency(self.fc)
		self.low_pass_filter_0.set_taps(firdes.low_pass(2, self.samp_rate, self.fc-self.fc/200, self.fc/100, firdes.WIN_HAMMING, 6.76))

	def get_dt_tgt1(self):
		return self.dt_tgt1

	def set_dt_tgt1(self, dt_tgt1):
		self.dt_tgt1 = dt_tgt1
		self.gr_delay_0.set_delay(self.dt_tgt1)

	def get_PRF(self):
		return self.PRF

	def set_PRF(self, PRF):
		self.PRF = PRF
		self.gr_sig_source_x_1.set_frequency(self.PRF)

if __name__ == '__main__':
	parser = OptionParser(option_class=eng_option, usage="%prog: [options]")
	(options, args) = parser.parse_args()
	tb = pulse_radar5kmrange()
	tb.Run(True)

